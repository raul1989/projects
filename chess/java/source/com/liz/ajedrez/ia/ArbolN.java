package com.liz.ajedrez.ia;

import java.util.ArrayList;
import java.util.List;

public class ArbolN<T> {
    public T elemento;
    public List<ArbolN<T>> hijos;

    public ArbolN() {
        this.hijos = new ArrayList<ArbolN<T>>();
    }

    public void agregar(ArbolN<T> elemento) {
        this.hijos.add(elemento);
    }
}