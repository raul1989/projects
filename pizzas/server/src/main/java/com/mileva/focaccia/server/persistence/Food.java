package com.mileva.focaccia.server.persistence;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="T_FOOD")
@TableGenerator(
    name="foodIDGenerator",
    pkColumnName="SEQ_NAME",
    valueColumnName="SEQ_ID",
    pkColumnValue="Food",
    initialValue=2048,
    allocationSize=1
)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class Food {
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="foodIDGenerator")    
    protected long id;
    protected String name;
    protected String description;
    protected float price;

    public Food() {}

    public Food(String name,
                String description,
                float price) {
        setName(name);
        setDescription(description);
        setPrice(price);
    }

    public void setId(long id) {this.id=id;}
    public void setName(String name) {this.name=name;}
    public void setDescription(String description) {this.description=description;}
    public void setPrice(float price) {this.price=price;}

    public long getId() {return id;}
    public String getName() {return name;}
    public String getDescription() {return description;}
    public float getPrice() {return price;}

}