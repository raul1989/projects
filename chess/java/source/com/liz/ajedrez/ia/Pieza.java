package com.liz.ajedrez.ia;

import java.util.List;

public abstract class Pieza {
    public boolean negra;
    public int indice;
    protected char nombre;
    public int valor;
    protected int posX;
    protected int posY;
    protected List<Posicion> posiciones;
    protected long bitboardPosiciones;

    public Pieza(boolean negra, char nombre, int valor, int posX, int posY) {
        this.negra = negra;
        this.nombre = nombre;
        this.valor = valor;
        this.posX = posX;
        this.posY = posY;
    }

    public abstract List<Posicion> calcularMovimientos(Tablero tablero);

    public int getPosX() {return this.posX;}
    public int getPosY() {return this.posY;}

    public void setPosX(int posX) {this.posX = posX;}
    public void setPosY(int posY) {this.posY = posY;}

    public String toString() {return Character.toString(this.nombre);}

}
