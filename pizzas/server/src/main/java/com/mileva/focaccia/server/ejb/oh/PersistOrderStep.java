package com.mileva.focaccia.server.ejb.oh;

import com.mileva.focaccia.server.persistence.Order;
import com.mileva.focaccia.server.persistence.Step;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@Local(StepLocal.class)
public class PersistOrderStep implements StepLocal {
    @EJB
    private OrderDBConnection db;

    public boolean run(Order order) {
        return handleResponse(order);
    }

    public boolean handleRequest(Order order) {
        return false;
    }

    /*
     * La orden debe venir con ID temporal que el cliente genera. Primero se debe buscar en la base de datos si ese
     * ID temporal ya existe. Si no existe, entonces persistir y marcar la actividad completada.
     * Si existe, entonces regresar ACK.
     */
    public boolean handleResponse(Order order) {
        System.out.println("Persisting Order...");
        order.getCurrentStep().setStatus(Step.STATUS_CO);
        order.setLoadNextStep(true);
        db.persist(order);
        return true;
    }

    public boolean handleACK(Order order) {
        return false;
    }
    
}