package com.liz.ajedrez.ia;

public class Tablero {
    public static int TAMANIO = 8;
    public Escaque[][] escaques;
    public Pieza[] negras;
    public Pieza[] blancas;
    public boolean turnoNegras;

    public Tablero() {
        this.escaques = new Escaque[TAMANIO][TAMANIO];
        for (int i = 0; i < TAMANIO; i++)
            for (int j = 0; j < TAMANIO; j++)
                this.escaques[i][j] = new Escaque();
        this.blancas = new Pieza[16];
        this.negras = new Pieza[16];
        construirPiezas();
        evaluarTablero(0);
        System.out.println("Tablero listo ...");
    }

    private void construirPiezas() {
        Pieza temp = null;
        int j = 1;
        //Se construyen los peones. Se agregan a su respectiva lista de piezas y
        //ademas se agregan al arreglo de escaques.
        for (int i = 0; i < 8; i++) {
        	j = 1;
            temp = new Peon(true, i, j);
            temp.indice = i;
            negras[i] = temp;
            escaques[i][j].pieza = temp;
            j = 6;
            temp = new Peon(false, i, j);
            temp.indice = i;
            blancas[i] = temp;
            escaques[i][j].pieza = temp;
        }
        //Se construyen las torres, caballos, dama y rey negros. 
        Pieza[] temps = new Pieza[] {new Torre(true, 0, 0),
                                     new Caballo(true, 1, 0),
                                     new Alfil(true, 2, 0),
                                     new Dama(true, 3, 0),
                                     new Rey(true, 4, 0),
                                     new Alfil(true, 5, 0),
                                     new Caballo(true, 6, 0),
                                     new Torre(true, 7, 0)};
        for (int i = 0; i < 8; i++) {
            negras[i + 8] = temps[i];
            negras[i + 8].indice = i + 8;
            escaques[i][0].pieza = temps[i];
        }
        //Se construyen las torres, caballos, dama y rey blancos. 
        temps = new Pieza[] {new Torre(false, 0, 7),
                             new Caballo(false, 1, 7),
                             new Alfil(false, 2, 7),
                             new Dama(false, 3, 7),
                             new Rey(false, 4, 7),
                             new Alfil(false, 5, 7),
                             new Caballo(false, 6, 7),
                             new Torre(false, 7, 7)};
        for (int i = 0; i < 8; i++) {
        	blancas[i + 8] = temps[i];
            blancas[i + 8].indice = i + 8;
            escaques[i][7].pieza = temps[i];
        }
    }

    public void evaluarTablero(int numJugada) {}
    
    public void actualizar(int x1, int y1, int x2, int y2) {
        Escaque eOrigen = escaques[x1][y1];
        Escaque eDestino = escaques[x2][y2];
        Pieza comida = eDestino.pieza;
        eDestino.pieza = eOrigen.pieza;
        eDestino.pieza.setPosX(x2);
        eDestino.pieza.setPosY(y2);
        eOrigen.pieza = null;
        if (comida != null) {
            if (comida.negra)
                negras[comida.indice] = null;
            else
                blancas[comida.indice] = null;
        }
    }

    public void actualizar(Pieza pieza, Posicion posicion) {
        Escaque eOrigen = escaques[pieza.getPosX()][pieza.getPosY()];
        Escaque eDestino = escaques[posicion.x][posicion.y];
        Pieza comida = eDestino.pieza;
        if (comida != null) {
        	if (comida.negra)
                negras[comida.indice] = null;
            else
                blancas[comida.indice] = null;
        }
        eOrigen.pieza = null;
        eDestino.pieza = pieza;
        pieza.setPosX(posicion.x);
        pieza.setPosY(posicion.y);
    }

	public Pieza getPieza(int x, int y) {
		return escaques[x][y].pieza;	
	}

    public Pieza getPieza(String posicion) {
    	char[] vals = posicion.toCharArray();
        int fila = vals[0] - 65;
        return escaques[vals[1] - 49][fila].pieza;
    }

    public String toString() {
        String tablero = "";
        for (int j = 0; j < Tablero.TAMANIO; j++) {
            for (int i = 0; i < Tablero.TAMANIO; i++) {
                if (escaques[i][j].pieza == null)
                    tablero = tablero + "#";
                else
                    tablero = tablero + escaques[i][j].pieza.toString();
            }
            tablero = tablero + "\n";
        }
        return tablero;
    }

    class Escaque {
        public Pieza pieza;
//            Int16 valor;
    }
}

class Estado {
    public Posicion piezaMovida;
	//En vez de guardar un objeto posicion, mejor una pieza?
    public Posicion piezaCapturada;

}
