package com.mileva.focaccia.server.ejb.oh;

import com.mileva.focaccia.server.persistence.Order;
import com.mileva.focaccia.server.persistence.Step;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class OrderHandler {
    public static final String STEP_INTERFACE="com.mileva.focaccia.server.ejb.oh.StepLocal";
    @Resource
    private SessionContext context;
    @EJB
    private OrderDBConnection db;

    /*
     *java:global/focaccia-1.0/PersistOrderStep!com.mileva.focaccia.server.ejb.StepLocal
     */
    public boolean handleOrder(Order order) {
        order = validate(order);
        if (order==null) return false;
        if (order.getCurrentStep()==null)
            prepareOrderForFirstRun(order);
        boolean executionState = lookupStep(order.getCurrentStep().getName()).run(order);
        loadNextStep(order);
        return executionState;
    }

    private Order validate(Order orderCandidate) {
        Order order = db.find(orderCandidate.getId());
        if (order==null)
            return orderCandidate;
        else if (!orderCandidate.getCurrentStep().getStatus().equals(order.getCurrentStep().getStatus()))
            return null;
        if (!db.contains(order))
            db.merge(order);
        return order;
    }


    public void prepareOrderForFirstRun(Order order) {
        Step step = new Step("PersistOrderStep", Step.friendlyNameMap.get("PersistOrderStep"), 0);
        order.setCurrentStep(step);
        order.addStep(step);
        order.setStatus(Order.STATUS_AC);
    }

    @Asynchronous
    public void loadNextStep(Order order) {
        Step step = null;
        String nameStep = null;
        if (order.getLoadNextStep()) {
            nameStep = OrderMaps.basicOrderMap.get(order.getCurrentStep().getName());
            step = new Step(nameStep,
                            Step.friendlyNameMap.get(nameStep),
                            0);
            order.setCurrentStep(step);
            order.addStep(step);
            order.setLoadNextStep(false);
            lookupStep(order.getCurrentStep().getName()).run(order);
        }
    }

    public StepLocal lookupStep(String stepName) {
        return (StepLocal) context.lookup("java:global/focaccia-1.0/" + stepName + "!" + STEP_INTERFACE);
    }


}