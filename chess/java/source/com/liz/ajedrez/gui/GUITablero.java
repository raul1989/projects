package com.liz.ajedrez.gui;

import com.liz.ajedrez.ia.Movimiento;
import com.liz.ajedrez.ia.Pieza;
import com.liz.ajedrez.ia.Posicion;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class GUITablero extends JPanel {
    private GUIEscaque[][] escaques;
    private Mouse mouse;
    
    public GUITablero() {
	mouse = new Mouse();
	crearAgregarEscaques();
	setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
	setPreferredSize(new Dimension(512, 512));	
    }
    
    private void crearAgregarEscaques() {
	boolean color = true;
	escaques = new GUIEscaque[8][8];
	for(int i = 0; i < 8; i++) {
	    for(int j = 0; j < 8; j++) {
		escaques[i][j] = new GUIEscaque(j, i);
		escaques[i][j].addMouseListener(mouse);
		escaques[i][j].setBorder(null);
		if (color) {
		    color = false;
		    //Color marfil
		    escaques[i][j].pintarFondo(new Color(255, 253, 208));	
		}else {
		    color = true;
		    //Color marron
		    escaques[i][j].pintarFondo(new Color(150, 75, 0));	
		}
		add(escaques[i][j]);
	    }
	    color = color ?  false : true;
	}
	try {
	    escaques[0][0].cambiarImagen(ImageIO.read(new File("../../recursos/torreM.png")));
	    escaques[0][1].cambiarImagen(ImageIO.read(new File("../../recursos/caballoM.png")));
	    escaques[0][2].cambiarImagen(ImageIO.read(new File("../../recursos/alfilM.png")));
	    escaques[0][3].cambiarImagen(ImageIO.read(new File("../../recursos/damaM.png")));
	    escaques[0][4].cambiarImagen(ImageIO.read(new File("../../recursos/reyM.png")));
	    escaques[0][5].cambiarImagen(ImageIO.read(new File("../../recursos/alfilM.png")));
	    escaques[0][6].cambiarImagen(ImageIO.read(new File("../../recursos/caballoM.png")));
	    escaques[0][7].cambiarImagen(ImageIO.read(new File("../../recursos/torreM.png")));

	    for(int i = 0; i < 8; i++) {
	    	escaques[1][i].cambiarImagen(ImageIO.read(new File("../../recursos/peonM.png")));	    
	    	escaques[6][i].cambiarImagen(ImageIO.read(new File("../../recursos/peonB.png")));
	    }
	    escaques[7][0].cambiarImagen(ImageIO.read(new File("../../recursos/torreB.png")));
	    escaques[7][1].cambiarImagen(ImageIO.read(new File("../../recursos/caballoB.png")));
	    escaques[7][2].cambiarImagen(ImageIO.read(new File("../../recursos/alfilB.png")));
	    escaques[7][3].cambiarImagen(ImageIO.read(new File("../../recursos/damaB.png")));
	    escaques[7][4].cambiarImagen(ImageIO.read(new File("../../recursos/reyB.png")));
	    escaques[7][5].cambiarImagen(ImageIO.read(new File("../../recursos/alfilB.png")));
	    escaques[7][6].cambiarImagen(ImageIO.read(new File("../../recursos/caballoB.png")));
	    escaques[7][7].cambiarImagen(ImageIO.read(new File("../../recursos/torreB.png")));
	    
	}catch(Exception e) {e.printStackTrace();}

    }

	class Mouse extends MouseAdapter {
		GUIEscaque anterior;
		GUIEscaque actual;
		List<GUIEscaque> esqs;
		boolean seleccion;

		public void mouseClicked(MouseEvent e) {
			List<Posicion> movs = null;
			Pieza pieza = null;
			Movimiento jugada = null;
			anterior = actual;
			actual = (GUIEscaque)(e.getComponent());
			if (seleccion) {
				for(GUIEscaque escaque : esqs)
					if (actual.getPosX() == escaque.getPosX() &&
						actual.getPosY() == escaque.getPosY()) {
						actual.cambiarImagen(anterior.getImagen());
						anterior.cambiarImagen(null);
						GUIAjedrez.ajedrez.actualizar(anterior.getPosX(), anterior.getPosY(),
													  actual.getPosX(), actual.getPosY());
						jugada = GUIAjedrez.ajedrez.jugar();
						escaques[jugada.posicion.y]
						        [jugada.posicion.x].cambiarImagen(escaques[jugada.pieza.getPosY()][jugada.pieza.getPosX()].getImagen());
						escaques[jugada.pieza.getPosY()]
								[jugada.pieza.getPosX()].cambiarImagen(null);
						GUIAjedrez.ajedrez.actualizar(jugada.pieza.getPosX(),
													  jugada.pieza.getPosY(),
													  jugada.posicion.x,
													  jugada.posicion.y);
						break;
					}
					seleccion = false;
					anterior.reiniciarFondo();
					for(GUIEscaque escaque : esqs)
						escaque.reiniciarFondo();
			}
			else if (actual.getImagen() != null) {
				seleccion = true;
				pieza = GUIAjedrez.ajedrez.getPieza(actual.getPosX(), actual.getPosY());
				movs = pieza.calcularMovimientos(GUIAjedrez.ajedrez.getTablero());
				actual.cambiarColor(Color.ORANGE);
				esqs = new ArrayList<GUIEscaque>();
				for(Posicion mov : movs) {
					esqs.add(escaques[mov.y][mov.x]);
					escaques[mov.y][mov.x].cambiarColor(Color.GREEN);
				}
			}
		}
	}
}
