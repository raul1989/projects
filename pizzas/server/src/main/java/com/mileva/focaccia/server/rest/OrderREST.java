package com.mileva.focaccia.server.rest;

import com.mileva.focaccia.server.ejb.oh.OrderDBConnection;
import com.mileva.focaccia.server.ejb.oh.OrderHandler;
import com.mileva.focaccia.server.persistence.Order;
import java.net.URI;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.PUT;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/orders")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrderREST {
    @Context
    private UriInfo uriInfo;
    @EJB
    private OrderHandler oh;   
    @EJB
    private OrderDBConnection db;

    @POST
    public Response createOrder(Order order) {
        System.out.println("Doing post...");
        oh.handleOrder(order);
        URI orderUri = uriInfo.getAbsolutePathBuilder().path("dummy").build();
        return Response.created(orderUri).build();
    }

    /*
     * Hay que armar una orden que tenga un ID y el nombre de un step con su respectivo estado.
     */
    @PUT
    public Response updateOrder(Order order) {
        System.out.print("Doing put... Order ID: " + order.getId() + " ");
        System.out.print("Step Name: " + order.getCurrentStep().getName() + " ");
        System.out.println("Step Status: " + order.getCurrentStep().getStatus());
        oh.handleOrder(order);
        return Response.ok(order).build();
    }

    @GET
    @Path("{id}")
    public Response getOrder(@PathParam("id") String id) {
        //delegar este metodo a orderhandler.
        Order order = db.find(Long.parseLong(id.trim()));
        if (order == null)
            throw new NotFoundException();
        return Response.ok(order).build();
    }
    
}