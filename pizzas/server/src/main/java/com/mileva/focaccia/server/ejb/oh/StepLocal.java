package com.mileva.focaccia.server.ejb.oh;

import com.mileva.focaccia.server.persistence.Order;
import javax.ejb.Local;

@Local
public interface StepLocal {
    boolean run(Order order);//En este paso siempre verificar que coincida el step con el que se ejecuta.
    boolean handleRequest(Order order);
    boolean handleResponse(Order order);
    boolean handleACK(Order order);
}