package com.mileva.focaccia.server.ejb.oh;

import com.mileva.focaccia.server.persistence.Order;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class OrderDBConnection {
    @PersistenceContext(name="fomdbunit1.0")
    private EntityManager em;

    public void persist(Order order) {
        em.persist(order);
    }

    public Order find(Long id) {
        return em.find(Order.class, id);
    }

    public void merge(Order order) {
        em.merge(order);
    }

    public boolean contains(Order order) {
        return em.contains(order);
    }
}