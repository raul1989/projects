package com.liz.ajedrez.gui;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JTextArea;

public class GUIBitacora extends JTextArea {
    
    public GUIBitacora() {
	setBackground(Color.BLACK);
	setPreferredSize(new Dimension(760, 200));
    }
}

