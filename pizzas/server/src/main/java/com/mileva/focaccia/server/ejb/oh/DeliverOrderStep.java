package com.mileva.focaccia.server.ejb.oh;

import com.mileva.focaccia.server.persistence.Order;
import com.mileva.focaccia.server.persistence.Step;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;

@Stateless
@Local(StepLocal.class)
public class DeliverOrderStep implements StepLocal {
    @EJB
    private OrderDBConnection db;

    public boolean run(Order order) {
        System.out.println("Running deliverOrderStep...");
        if (!db.contains(order))
            db.merge(order);
        switch(order.getCurrentStep().getStatus()) {
            case Step.STATUS_AC:
                return handleRequest(order);
            case Step.STATUS_AW:
                return handleResponse(order);
            case Step.STATUS_SF:
                return handleACK(order);
        }
        return false;
    }

    public boolean handleRequest(Order order) {
        order.getCurrentStep().setStatus(Step.STATUS_AW);
        Client client = ClientBuilder.newClient();
        client
        .target("https://gcm-http.googleapis.com/gcm/send")
        .request()
        .header("Authorization", "key=AIzaSyAGVdZrxnt7T8OBLTiUtvMegxDm5tRJqXI")
        .post(Entity.json(createJSONMessage(order)));
        System.out.println("[SendOrderStep] Request to Focaccia has been sent.");
        return true;
    }

    private String createJSONMessage(Order order) {
        StringBuilder JSONMessage = new StringBuilder();
        JSONMessage
        .append("{\"to\":\"/topics/focacciachef\",\"data\":{")
        .append("\"orderID\":\"" + order.getId() + "\",")
        .append("\"steps\":[");
        for(Step step : order.getSteps())
            JSONMessage.append("{\"name\":\"" + step.getName() +
            "\",\"status\":\"" + step.getStatus() +
            "\",\"friendlyName\":\"" + step.getFriendlyName() + "\"},");
        JSONMessage
        .replace(JSONMessage.length()-1, JSONMessage.length(), "]")
        .append("}}");
        System.out.println(JSONMessage);
        return JSONMessage.toString();
    }

    /*
     * Aqui se debe avisar al cliente que Focaccia ya recibio la orden. Se debe intentar hasta que el cliente confirme.
     */
    public boolean handleResponse(Order order) {
        order.getCurrentStep().setStatus(Step.STATUS_SF);

        StringBuilder JSONMessage = new StringBuilder();
        JSONMessage
        .append("{\"to\":\"/topics/focaccia\",\"data\":{")
        .append("\"orderID\":\"" + order.getId() + "\",")
        .append("\"step\":[")
        .append("{\"name\":\"" + order.getCurrentStep().getName() + "\",\"status\":\"" + order.getCurrentStep().getStatus() + "\"}]}}");
        Client client = ClientBuilder.newClient();
        client
        .target("https://gcm-http.googleapis.com/gcm/send")
        .request()
        .header("Authorization", "key=AIzaSyC-c7CsD6bkV9jasJVNpWVzXjQL427k1SY")
        .post(Entity.json(JSONMessage.toString()));
        System.out.println("[SendOrderStep] Response from Focaccia has been sent to the customer. Step status: " + order.getCurrentStep().getStatus());
        return true;
    }

    public boolean handleACK(Order order) {
        order.getCurrentStep().setStatus(Step.STATUS_CO);
        order.setLoadNextStep(true);
        return true;
    }
}