package com.liz.ajedrez.ia;

import java.util.ArrayList;
import java.util.List;

public class Alfil extends Pieza {
    public Alfil(boolean negra, int posX, int posY) {
        super(negra, negra ? 'a' : 'A', 150, posX, posY);
    }

    public List<Posicion> calcularMovimientos(Tablero tablero) {
        posiciones = new ArrayList<Posicion>();
        for (int i = posX + 1, j = posY + 1; i < Tablero.TAMANIO && j < Tablero.TAMANIO; i++, j++) {
            if (tablero.escaques[i][j].pieza == null)
                posiciones.add(new Posicion(i, j));
            else if (negra && !tablero.escaques[i][j].pieza.negra) {
                posiciones.add(new Posicion(i, j));
                break;
            }
            else if (negra && tablero.escaques[i][j].pieza.negra)
                break;
            else if (!negra && tablero.escaques[i][j].pieza.negra) {
                posiciones.add(new Posicion(i, j));
                break;
            }
    		else if (!negra && !tablero.escaques[i][j].pieza.negra)
                break;
            }

        for (int i = posX - 1, j = posY - 1; i >= 0 && j >= 0; i--, j--) {
            if (tablero.escaques[i][j].pieza == null)
                posiciones.add(new Posicion(i, j));
            else if (negra && !tablero.escaques[i][j].pieza.negra) {
                posiciones.add(new Posicion(i, j));
                break;
            }
            else if (negra && tablero.escaques[i][j].pieza.negra)
                break;
            else if (!negra && tablero.escaques[i][j].pieza.negra) {
                posiciones.add(new Posicion(i, j));
                break;
            }
            else if (!negra && !tablero.escaques[i][j].pieza.negra)
                break;
        }

        for (int i = posX + 1, j = posY - 1; i < Tablero.TAMANIO && j >= 0; i++, j--) {
            if (tablero.escaques[i][j].pieza == null)
                posiciones.add(new Posicion(i, j));
            else if (negra && !tablero.escaques[i][j].pieza.negra) {
                posiciones.add(new Posicion(i, j));
                break;
            }
            else if (negra && tablero.escaques[i][j].pieza.negra)
                break;
            else if (!negra && tablero.escaques[i][j].pieza.negra) {
                posiciones.add(new Posicion(i, j));
                break;
            }
            else if (!negra && !tablero.escaques[i][j].pieza.negra)
                break;
        }

        for (int i = posX - 1, j = posY + 1; i >= 0 && j < Tablero.TAMANIO; i--, j++) {
            if (tablero.escaques[i][j].pieza == null)
                posiciones.add(new Posicion(i, j));
            else if (negra && !tablero.escaques[i][j].pieza.negra) {
                posiciones.add(new Posicion(i, j));
                break;
            }
			else if (negra && tablero.escaques[i][j].pieza.negra)
                break;
            else if (!negra && tablero.escaques[i][j].pieza.negra) {
                posiciones.add(new Posicion(i, j));
                break;
            }
            else if (!negra && !tablero.escaques[i][j].pieza.negra)
                 break;
        }
        return posiciones;
    }

}
