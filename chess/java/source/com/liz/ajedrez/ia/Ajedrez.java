package com.liz.ajedrez.ia;

import java.util.List;

public class Ajedrez {
    public static int PROFUNDIDAD = 5;
    public static int INFINITO = 1000000;
    public static double COEFICIENTE_MATERIAL = 78.0;
    public static double COEFICIENTE_ATAQUE = 22.0;
    public static double TOTAL_MATERIAL = 3220.0;
    public static double TOTAL_ATAQUE = 16.0;
    public static double COCIENTE_MATERIAL = COEFICIENTE_MATERIAL / TOTAL_MATERIAL;
    public static double COCIENTE_ATAQUE = COEFICIENTE_ATAQUE / TOTAL_ATAQUE;
    private Tablero tablero;
    private int numJugada;
    private int hojas;
    public ArbolN<Movimiento> arbol;
    
    public Ajedrez() {
        this.tablero = new Tablero();
        this.arbol = new ArbolN<Movimiento>();
    }
    
    public Movimiento jugar() {
    	hojas = 0;
	long tInicial = System.currentTimeMillis();
        Movimiento mov = alfaBeta(true, 0, new Movimiento(null, null, -INFINITO),
				  new Movimiento(null, null, INFINITO));
        System.out.println("Log: el mejor movimiento e$: " + mov + " con v@lor: " + mov.valor);
        System.out.println("Log: numero de hojas: " + hojas);
        System.out.println("Log: tiempo: " + (System.currentTimeMillis() - tInicial));
	return mov;
    }
    
    public Pieza getPieza(int x, int y) {
	return tablero.getPieza(x, y);	
    }	
    
    public void actualizar(int x1, int y1, int x2, int y2) {
	this.tablero.actualizar(x1, y1, x2, y2);
    }
    
    public void actualizar(Pieza pieza, Posicion destino) {
        this.tablero.actualizar(pieza, destino);        
    }
    
    /*
     * Implementacion del algoritmo poda alfabeta.
     */
    private Movimiento alfaBeta(boolean turnoNegras, int profundidad, Movimiento alfa, Movimiento beta) {
        if (profundidad == PROFUNDIDAD) {
	    hojas++;
            return new Movimiento(null, null, evaluarMovimiento());
        }
        Pieza[] piezas = (turnoNegras) ? tablero.negras : tablero.blancas;
        List<Posicion> posiciones = null;
        Estado estado = new Estado();
	//Es mejor apuntar desde estado a la pieza capturada.
        Pieza piezaCapturada = null;
        Movimiento movimientoAux = null;
	boolean podar = false;
        numJugada++;
        for(Pieza pieza : piezas) {
	    if (podar)
		break;
	    if (pieza != null) {
            	posiciones = pieza.calcularMovimientos(tablero);
            	estado.piezaMovida = new Posicion(pieza.getPosX(), pieza.getPosY());
            	tablero.escaques[estado.piezaMovida.x][estado.piezaMovida.y].pieza = null;
            	for(Posicion posicion : posiciones) {
		    //codigo de poda.
		    if (podar)
			break;
		    //Se guarda el estado del tablero.
		    pieza.setPosX(posicion.x);
		    pieza.setPosY(posicion.y);
		    if (tablero.escaques[posicion.x][posicion.y].pieza != null) {
                    	piezaCapturada = tablero.escaques[posicion.x][posicion.y].pieza;
                    	estado.piezaCapturada = new Posicion(posicion.x, posicion.y);
                    	if (turnoNegras)
			    tablero.blancas[piezaCapturada.indice] = null;
                    	else
			    tablero.negras[piezaCapturada.indice] = null;
		    }
		    tablero.escaques[posicion.x][posicion.y].pieza = pieza;
		    movimientoAux = alfaBeta((turnoNegras) ? false : true, profundidad + 1,
					     new Movimiento(null, null, alfa.valor),
					     new Movimiento(null, null, beta.valor));
		    movimientoAux.pieza = pieza;
		    movimientoAux.posicion = posicion;
		    if (turnoNegras)
			alfa = calcMAX(alfa, movimientoAux);
		    else
			beta = calcMIN(beta, movimientoAux);
		    if (beta.valor <= alfa.valor) {
			podar = true;
		    }
		    //Se restaura el estado del tablero.
		    if (piezaCapturada != null) {
			tablero.escaques[estado.piezaCapturada.x]
			    [estado.piezaCapturada.y].pieza = piezaCapturada;
			if (turnoNegras)
			    tablero.blancas[piezaCapturada.indice] = piezaCapturada;
			else
			    tablero.negras[piezaCapturada.indice] = piezaCapturada;
                    }
		    else
			tablero.escaques[posicion.x][posicion.y].pieza = null;
		    piezaCapturada = null;
		}//Fin del ciclo de las posiciones.
		pieza.setPosX(estado.piezaMovida.x);
		pieza.setPosY(estado.piezaMovida.y);
            	tablero.escaques[estado.piezaMovida.x]
		    [estado.piezaMovida.y].pieza = pieza;
	    }
	    numJugada--;
    	}//Fin del ciclo de las piezas.
    	return turnoNegras ? alfa : beta;
    }
    
    /*
     * Funcion de evaluacion.
     */
    private double evaluarMovimiento() {
        int numNegras = 0;
        int numBlancas = 0;
	int ataqueNegras = 0;
	int ataqueBlancas = 0;
	int x1Radio = 0;
	int x2Radio = 0;
	int y1Radio = 0;
	int y2Radio = 0;
	int offsetx1, offsetx2, offsety1, offsety2;
	
	//Ventaja material y de ataque para negras.
	if (tablero.blancas[12] != null) {
	    x1Radio = tablero.blancas[12].posX - 2;
	    x2Radio = tablero.blancas[12].posX + 2;
	    y1Radio = tablero.blancas[12].posY - 2;
	    y2Radio = tablero.blancas[12].posY + 2;
	    for(Pieza pieza : tablero.negras) {
		if (pieza != null) {
		    numNegras += pieza.valor;
		    /********************** 
		     * Ataque de negras
		     **********************/
		    if (x2Radio > Tablero.TAMANIO) {
			offsetx2 = x2Radio - Tablero.TAMANIO;
			x1Radio -= offsetx2; 
			x2Radio = 7;
		    }
		    if (x1Radio < 0) {
			offsetx1 = 0 - x1Radio;
			x2Radio = x2Radio + offsetx1;
			x1Radio = 0;
		    }
		    if (y2Radio > Tablero.TAMANIO) {
			offsety2 = y2Radio - Tablero.TAMANIO;
			y1Radio -= offsety2;
			y2Radio = 7;
		    }
		    if (y1Radio < 0) {
			offsety1 = 0 - y1Radio;
			y2Radio = y2Radio + offsety1;
			y1Radio = 0;
		    }
		    if ((pieza.posX >= x1Radio && pieza.posX <= x2Radio) &&
			(pieza.posY >= y1Radio && pieza.posY <= y2Radio))
			ataqueNegras++;
		    /**************************
		     * Fin de ataque de negras
		     **************************/
		}
	    }
	}
	else
	    for(Pieza pieza : tablero.negras)
            	if (pieza != null)
		    numNegras += pieza.valor;
	
	//Ventaja material y de ataque para blancas.
	if (tablero.negras[12] != null) {
	    x1Radio = tablero.negras[12].posX - 2;
	    x2Radio = tablero.negras[12].posX + 2;
	    y1Radio = tablero.negras[12].posY - 2;
	    y2Radio = tablero.negras[12].posY + 2;
	    for(Pieza pieza : tablero.blancas) {
            	if (pieza != null) {
		    numBlancas += pieza.valor;
		    /********************** 
		     * Ataque de blancas
		     **********************/
		    if (x2Radio > Tablero.TAMANIO) {
			offsetx2 = x2Radio - Tablero.TAMANIO;
			x1Radio -= offsetx2; 
			x2Radio = 7;
		    }
		    if (x1Radio < 0) {
			offsetx1 = 0 - x1Radio;
			x2Radio = x2Radio + offsetx1;
			x1Radio = 0;
		    }
		    if (y2Radio > Tablero.TAMANIO) {
			offsety2 = y2Radio - Tablero.TAMANIO;
			y1Radio -= offsety2;
			y2Radio = 7;
		    }
		    if (y1Radio < 0) {
			offsety1 = 0 - y1Radio;
			y2Radio = y2Radio + offsety1;
			y1Radio = 0;
		    }
		    if ((pieza.posX >= x1Radio && pieza.posX <= x2Radio) &&
			(pieza.posY >= y1Radio && pieza.posY <= y2Radio))
			ataqueBlancas++;
		    /**************************
		     * Fin de ataque de blancas
		     **************************/
                }
            }
        }else
	    for(Pieza pieza : tablero.blancas)
            	if (pieza != null)
		    numBlancas += pieza.valor;
        
        return (numNegras - numBlancas) * COCIENTE_MATERIAL + 
	    (ataqueNegras - ataqueBlancas) * COCIENTE_ATAQUE;
    }
    
    private boolean hayJaque() {return false;}
    
    private boolean hayJaqueMate() {
	/*
	 * Verificar que el Rey esta en la lista de movimientos de la pieza que
	 * esta jaqueando.
	 * Verificar que los movimientos del Rey son alcanzables por el contrincante.
	 * Verificar que los movimientos de la pieza que jaquea sean alcanzables
	 * por el contrincante.
	 */
    	return false;
    }
    
    private Movimiento calcMAX(Movimiento mov1, Movimiento mov2) {
	return mov1.valor < mov2.valor ? mov2 : mov1; 
    }
    
    private Movimiento calcMIN(Movimiento mov1, Movimiento mov2) {
	return mov1.valor > mov2.valor ? mov2 : mov1;	
    }
    
    private void recorrerArbol(ArbolN<Movimiento> raiz) {
        List<ArbolN<Movimiento>> movimientos = raiz.hijos;
        for(ArbolN<Movimiento> movimiento : movimientos) {
            System.out.println("Log: " + movimiento.elemento);
        }
    }
    
    public Tablero getTablero() {return tablero;}
    
    /*
      public static void main(String[] args) {
      Ajedrez ajedrez = new Ajedrez();
      String jugada = null;
      System.out.println(ajedrez.tablero);
      System.out.print("Tu jugada: ");
      while (!(jugada = System.console().readLine()).equals("salir")) {
      String[] posiciones = jugada.split(" ");
      ajedrez.actualizar(posiciones[0], posiciones[1]);
      Movimiento mov = ajedrez.jugar();
      ajedrez.actualizar(mov.pieza, mov.posicion);
      System.out.println(ajedrez.tablero);
      ajedrez.arbol = new ArbolN<Movimiento>();
      System.out.print("Tu jugada: ");
      }
      //            ajedrez.RecorrerArbol(ajedrez.arbol);
      //            Console.ReadLine();
      }
    */
}