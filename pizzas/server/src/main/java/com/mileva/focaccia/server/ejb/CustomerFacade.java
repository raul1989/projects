package com.mileva.focaccia.server.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class CustomerFacade {
    @PersistenceContext(name="fomdbunit1.0")
    private EntityManager em;

    public <T> T persistEntity(T entity) {
        em.persist(entity);
        return entity;
    }
    
}