package com.liz.ajedrez.ia;

import java.util.ArrayList;
import java.util.List;

public class Peon extends Pieza {

	public Peon(Boolean negra, int posX, int posY) {
        super(negra, negra ? 'p' : 'P', 85, posX, posY);
        this.negra = negra;
	}

    public List<Posicion> calcularMovimientos(Tablero tablero) {
        List<Posicion> posts = new ArrayList<Posicion>();
        int j = 0, i = 0;
        if (negra) {
            j = posY + 1;
            if (j < Tablero.TAMANIO)
                if (tablero.escaques[posX][j].pieza == null)
                    posts.add(new Posicion(posX, j));
            if (posY == 1) {
                j = posY + 2;
                if (j < Tablero.TAMANIO)
                    if ((tablero.escaques[posX][j].pieza == null) &&
                    	(tablero.escaques[posX][j - 1].pieza == null))
                         posts.add(new Posicion(posX, j));
            }
            i = posX + 1;
            j = posY + 1;
            if ((i < Tablero.TAMANIO) && (j < Tablero.TAMANIO))
                if ((tablero.escaques[i][j].pieza != null) && (!tablero.escaques[i][j].pieza.negra))
                    posts.add(new Posicion(i, j));
            i = posX - 1;
            j = posY + 1;
            if ((i >= 0) && (j < Tablero.TAMANIO))
                if ((tablero.escaques[i][j].pieza != null) && (!tablero.escaques[i][j].pieza.negra))
                    posts.add(new Posicion(i, j));
                //movimiento para capturar pieza.
        }else {
            j = posY - 1;
            if (0 <= j)
                if (tablero.escaques[posX][j].pieza == null)
                    posts.add(new Posicion(posX, j));
            if (posY == 6) {
            	j = posY - 2;
                if (0 <= j)
                    if ((tablero.escaques[posX][j].pieza == null) &&
                        (tablero.escaques[posX][j + 1].pieza == null)) 
                        posts.add(new Posicion(posX, j));
            }
            i = posX + 1;
            j = posY - 1;
            //Se sale del tablero.
            if ((i < Tablero.TAMANIO) && (j >= 0))
                if ((tablero.escaques[i][j].pieza != null) && (tablero.escaques[i][j].pieza.negra)) {
                    posts.add(new Posicion(i, j));
                }
            i = posX - 1;
            j = posY - 1;
            if ((i >= 0) && (j >= 0))
                if ((tablero.escaques[i][j].pieza != null) && (tablero.escaques[i][j].pieza.negra)) {
                    posts.add(new Posicion(i, j));
                }
        }
        return posts;
    }
    
}
