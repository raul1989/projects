package com.mileva.focaccia.server.rest;

import com.mileva.focaccia.server.ejb.FoodFacade;
import com.mileva.focaccia.server.persistence.Beverage;
import com.mileva.focaccia.server.persistence.Dish;
import com.mileva.focaccia.server.persistence.Food;
import com.mileva.focaccia.server.persistence.FoodList;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/menu")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FoodREST {
    @EJB
    private FoodFacade foodFacade;
    @Context
    private UriInfo uriInfo;

    @POST
    @Path("/dish")
    public Response createDish(Dish dish) {
        return createFood("dish", dish);
    }

    @POST
    @Path("/beverage")
    public Response createBeverage(Beverage beverage) {
        return createFood("beverage", beverage);
    }

    private Response createFood(String type, Food food) {
        if (food==null)
            throw new BadRequestException();
        foodFacade.persistEntity(food);
        URI foodUri = uriInfo.getAbsolutePathBuilder().path(type + "/" + Long.toString(food.getId())).build();
        return Response.created(foodUri).build();
    }

    @GET
    @Path("/{type}/{id}/")
    public Response getFood(@PathParam("type") String type, @PathParam("id") String id) {
        Food food = null;
            //Checar que no es lo mismo la string que la clase
//            food = foodFacade.getFoodFindById((Class<Food>)(Class.forName(type)), Long.parseLong(id.trim()));
            food = foodFacade.getFoodFindById(Dish.class, Long.parseLong(id.trim()));

            if (food == null)
                throw new NotFoundException();

        return Response.ok(food).build();
    }

    @GET
    @Path("/dishes")
    public Response getAllDishes() {
        return getAllFood("Dish", Dish.class);
    }    
    
    @GET
    @Path("/beverages")
    public Response getAllBeverages() {
        return getAllFood("Beverage", Beverage.class);
    }
    
    private <T extends Food> Response getAllFood(String nameClass, Class<T> foodClass) {
        List<T> foodList = foodFacade.getFoodFindAll(nameClass, foodClass);
        return Response.ok(new FoodList(foodList)).build();
    }
    
}